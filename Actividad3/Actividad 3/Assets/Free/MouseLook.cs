using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public GameObject camerasParent; //Parent object of all cameras that should rotate with mouse
    public float hRotationSpeed = 100f; //Player rotates along y axis
    public float vRotationSpeed = 80f;// cam rotates along x axis
    public float maxVerticalAngle; //maximum rotation along x axis
    public float minVerticalAngle;//minimum rotation along x axis
    public float smoothTime = 0.05f;

    float vCamRotationAngles;//variable to apply Vertical Rotation 
    float hPlayerRotation;//variable to apply Horizontal Rotation
    float currentHVelocity;//smooth horizontal velocity
    float currentVVelocity;//smooth vertical velocity
    float targetCamEulers;//variable to accumulate the eurler angles along x axis
    Vector3 targetCamRotation;



    void Start()  {
        //hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        
    }

    void Update() {
        //get mouse input
        targetCamEulers += Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float targetPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;
        //Player Rotation
        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref currentHVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRotation, 0f);
        //Cam rotation
        targetCamEulers = Mathf.Clamp(targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles, targetCamEulers, ref currentVVelocity, smoothTime);

        targetCamRotation.Set(-vCamRotationAngles, 0f, 0f);
        camerasParent.transform.localEulerAngles = targetCamRotation;
        
    }

    internal void handleRotation(float hRotationInput, float vRotationInput)
    {
        throw new NotImplementedException();
    }
}
